# Codelocks Logger #

this is a util for write a specification logging 

## How to use ##
```
npm install codelocks-logging
```
```javascript
const Logger = require('codelocks-logging');
// create a logger with APP: LMB, File: IDX
const logger = Logger.getLogger('LMB', 'IDX')
// write logging in function test
logger.info('TST','0001', 'this is a message');
// console 
// INFO - 2018-09-12 05:48:34 ---> this is a test message||{"Level":"ERROR","Location":"LMB/LGT/TST/0001","Message":"this is a test message"}
logger.error('TST','0001', 'test a error message', err);
// ERROR - 2018-09-12 05:48:34 ---> test a error message||{"Level":"ERROR","Location":"LMB/LGT/TST/0001","Message":"test a error message","Detail":{"message":"test a error message","stack":"Error:……}}

// if we dont want write more message
logger.error('TST','0001', err);
// it will same as logger.error('TST','0001', err.message, err);
logger.error('TST','0001', { message:'test' });
// it will same as logger.error('TST','0001', 'test', {message:'test'});

```