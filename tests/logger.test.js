const Logger = require('../index');

describe('src/logger.js', () => {
  let logger;
  beforeEach(() => {
    logger = Logger.getLogger('lmb', 'lgt');
  });
  test('log', () => {
    const EX_TEMP = /^(INFO|ERROR) - \d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2} ---> .*?\|\|.*?$/;
    let result = logger.info('tst', '0001', 'this is a test message');
    expect(result).toMatch(EX_TEMP);
    result = logger.error('tst', '0001', 'this is a test message2');
    expect(result).toMatch(EX_TEMP);
    result = logger.error('tst', '0001', 'this is a test message2');
    expect(result).toMatch(EX_TEMP);
    result = logger.error('tst', '0001', new Error('test a error message'));
    expect(result).toMatch(EX_TEMP);
    result = logger.info('tst', '0001', { message: 'test message' });
    expect(result).toMatch(EX_TEMP);
  });
});
