

const { format } = require('util');
const moment = require('moment');
/**
 * @class Logger
 */
class Logger {
  /**
   * Creates new instance of Logger
   * @constructor
   * @param  {Object} options Optional options object to override
   * @param  {Object} levels  Optional log level configuration object
   * @return {this}           Instance of LambdaLog
   */
  constructor(options = {}, levels = {}) {
    this.options = Object.assign({
      // Global Location identifier of <APP>,
      // Must be any one of: API-5 (Main API); LMB (Lambda functions)
      app: 'LMB',
      // Global Location indentifier of File
      // File
      //     API-5: 3 character abbreviation of the file name
      //     LMB: 3 character abbreviation of the Lambda function name
      file: 'IDX',
      levels: Object.assign({
        info: 'info',
        billing: 'info',
        debug: 'log',
        debugcl: 'log',
        warn: 'warn',
        error: 'error',
      }, levels),
    }, options);
    this.levels = Object.keys(this.options.levels);
    this.levels.forEach((lvl) => {
      this[lvl] = this.log.bind(this, lvl);
    });
  }

  /**
   * Creates log message based on the provided parameters
   * @param  {string} method 3 character abbreviation of the function/method name.
   * @param  {string} location 4 numeric character string (with leading zeros),
   *         incremental within the function/method.
   * @param  {any}  message Message to log. Max characters,
   *         if detail is undefind, and message is a object.
   *         message will be set of detail,and message shoud be like {message:'a message'}
   * @param  {any}  detail A JSON Object, Message detail. it can be undefined
   */
  log(level, method, location, message, detail) {
    if (this.levels.indexOf(level) === -1) {
      throw new Error(`"${level}" is not a valid log level`);
    }
    const {
      app, file, levels, silent,
    } = this.options;

    const content = {
      Level: level.toUpperCase(),
      Location: `${app.toUpperCase()}/${file.toUpperCase()}/${method.toUpperCase()}/${location}`,
      Message: message,
      Detail: detail,
    };
    if (!detail && typeof message === 'object') {
      content.Message = message.message;
      content.Detail = message;
    }
    if (content.Detail instanceof Error) {
      content.Detail = {
        message: content.Detail.message,
        stack: content.Detail.stack,
        name: content.Detail.name,
        code: content.Detail.code,
      };
    }
    const template = '%s - %s ---> %s||%s';
    const date = moment().utc().format('YYYY-MM-DD HH:mm:ss');
    const logMessage = format(template,
      level.toUpperCase(), date, content.Message, JSON.stringify(content));
    const lvlMethod = levels[level];
    if (!lvlMethod) return false;

    if (!silent) {
      console[lvlMethod](logMessage);
    }
    return logMessage;
  }
}
module.exports = Logger;
