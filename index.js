
const Logger = require('./src/logger');
/**
 *
 * @param {string} app
 * @param {string} file
 */
Logger.getLogger = (app, file) => new Logger({ app, file });

module.exports = Logger;
